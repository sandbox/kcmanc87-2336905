/**
 * @file
 */
(function ($) {
    Drupal.behaviors.WOWJS = {
        attach: function (context, settings) {
            new WOW().init();
        }
    };
})(jQuery);