core = 7.x
api = 2
;Libraries
;Animate.CSS
libraries[animate.css][download][type] = "git"
libraries[animate.css][download][url] = "https://github.com/daneden/animate.css.git"
libraries[animate.css][download][destination] = "libraries"
libraries[animate.css][download][directory_name] = "animate.css"

;WOW
libraries[WOW][download][type] = "git"
libraries[WOW][download][url] = "https://github.com/matthieua/WOW.git"
libraries[WOW][download][destination] = "libraries"
libraries[WOW][download][directory_name] = "WOW"